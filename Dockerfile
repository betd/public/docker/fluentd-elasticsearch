ARG VERSION=latest
FROM fluent/fluentd:${VERSION}
USER root
# https://docs.fluentd.org/output/elasticsearch
RUN ["gem", "install", "fluent-plugin-elasticsearch", "--no-doc"]
RUN ["gem", "install", "fluent-plugin-concat", "--no-doc"]
RUN ["gem", "install", "fluent-plugin-detect-exceptions", "--no-doc"]
RUN ["gem", "install", "fluent-plugin-rewrite-tag-filter", "--no-doc"]
USER fluent
ENTRYPOINT ["fluentd", "-c", "/fluentd/etc/fluent.conf"]